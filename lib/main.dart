import 'package:AmarZakat/Controller/status_bar.dart';
import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/routes.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  GestureBinding.instance.resamplingEnabled = true;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: AppBarTheme(color: ConstColors.homeAppsBar),
        // scaffoldBackgroundColor: ConstColors.screenbg,
        //primarySwatch: ConstColors.primary,
        fontFamily: "kalpurush",
        backgroundColor: ConstColors.screenbg,
      ),
      // home: ContertPage2(),
      initialRoute: MyRoutes.splash_screen_widget,
      getPages: routes(),
      // transitionDuration: Duration(milliseconds: 300),
    );
  }
}
