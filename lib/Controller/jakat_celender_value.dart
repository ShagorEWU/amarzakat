import 'package:get/get.dart';

class JakatCelenderValueController extends GetxController {
  final dAY = "3".obs;
  final mONTH = "February".obs;
  final yEAR = "2019".obs;
  final wEEK = "Sunday".obs;
}
