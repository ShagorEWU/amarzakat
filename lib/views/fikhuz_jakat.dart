import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/page_transition.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:AmarZakat/utils/widgets/my_text.dart';
import 'package:AmarZakat/views/search_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class FikahZakatDetailsPage extends StatefulWidget {
  @override
  _FikahZakatDetailsPageState createState() => _FikahZakatDetailsPageState();
}

class _FikahZakatDetailsPageState extends State<FikahZakatDetailsPage> {
  final String title = Get.arguments as String;

  List<String> dataList = [
    "আল্লাহর নাম নিয়ে  قَالَ رَجُلٌ مِنْ حَضْرَمَوْتَ مَا الْحَدَثُ يَا أَبَا (আরম্ভ করছি), (যিনি) রহমান (--পরম করুণাময়, যিনি অসীম করুণা ও দয়া বশতঃ বিশ্বজগতের সমস্ত সৃষ্টির সহাবস্থানের প্রয়োজনীয় সব ব্যবস্থা অগ্রিম করে রেখেছেন), (যিনি) রহীম (--অফুরন্ত ফলদাতা, যাঁর অপার করুণা ও দয়ার ফলে প্রত্যেকের ক্ষুদ্রতম শুভ-প্রচেষ্টাও বিপুলভাবে সাফল্যমণ্ডিত ও পুরস্কৃত হয়ে থাকে) ٱلْحَمْدُ لِلَّهِ رَبِّ ٱلْعَٰلَمِينَ সমস্ত প্রশংসা আল্লাহ্‌র প্রাপ্য, সমুদয় সৃষ্ট-জগতের রব্ব।",
    "aحَدَّثَنَا إِسْحَاقُ بْنُ إِبْرَاهِيمَ الْحَنْظَلِيُّ، قَالَ أَخْبَرَنَا عَبْدُ الرَّزَّاقِ، قَالَ أَخْبَرَنَا مَعْمَرٌ، عَنْ هَمَّامِ بْنِ مُنَبِّهٍ، أَنَّهُ سَمِعَ أَبَا هُرَيْرَةَ، يَقُولُ قَالَ رَسُولُ اللَّهِ صلى الله عليه وسلم ‏  ‏ لاَ تُقْبَلُ صَلاَةُ مَنْ أَحْدَثَ حَتَّى يَتَوَضَّأَ  .‏ قَالَ رَجُلٌ مِنْ حَضْرَمَوْتَ مَا الْحَدَثُ يَا أَبَا هُرَيْرَةَ قَالَ فُسَاءٌ أَوْ ضُرَاطٌ‏",
    "তিনি বলেনঃ আল্লাহ্‌র রসূল (সাল্লাল্লাহু ‘আলাইহি ওয়া সাল্লাম) বলেছেনঃ ‘যে ব্যক্তির হাদাস হয় তার সালাত কবুল হবে না, যতক্ষণ না সে উযূ করে। হাযরা-মাওতের জনৈক ব্যক্তি বললো, ‘হে আবূ হুরাইরা! হাদাস কী? হাদাস কী?’ তিনি বললেন, ‘নিঃশব্দে বা সশব্দে বায়ু বের হওয়া।’ \n(৬৯৫৪; মুসলিম ২/২, হাঃ ২২৫, আহমাদ ৮০৮৪) (আধুনিক প্রকাশনীঃ ১৩২, ইসলামী ফাউন্ডেশনঃ ১৩৭) ",
    "aحَدَّثَنَا يَحْيَى بْنُ بُكَيْرٍ، قَالَ حَدَّثَنَا اللَّيْثُ، عَنْ خَالِدٍ، عَنْ سَعِيدِ بْنِ أَبِي هِلاَلٍ، عَنْ نُعَيْمٍ الْمُجْمِرِ، قَالَ رَقِيتُ مَعَ أَبِي هُرَيْرَةَ عَلَى ظَهْرِ الْمَسْجِدِ، فَتَوَضَّأَ فَقَالَ إِنِّي سَمِعْتُ النَّبِيَّ صلى الله عليه وسلم يَقُولُ‏ إِنَّ أُمَّتِي يُدْعَوْنَ يَوْمَ الْقِيَامَةِ غُرًّا مُحَجَّلِينَ مِنْ آثَارِ الْوُضُوءِ، فَمَنِ اسْتَطَاعَ مِنْكُمْ أَنْ يُطِيلَ غُرَّتَهُ فَلْيَفْعَلْ",
    "নু’আয়ম মুজমির (রহঃ) থেকে বর্ণিতঃ তিনি বলেন, আমি আবূ হুরায়রা (রাঃ)-এর সঙ্গে মসজিদের ছাদে উঠলাম। অতঃপর তিনি উযূ করে বললেনঃ ‘আমি আল্লাহ্‌র রাসূল (সাল্লাল্লাহু ‘আলাইহি ওয়া সাল্লাম)-কে বলতে শুনেছি, ক্বিয়ামাতের দিন আমার উম্মাতকে এমন অবস্থায় আহ্বান করা হবে যে, উযূর প্রভাবে তাদের হাত-পা ও মুখমন্ডল উজ্জ্বল থাকবে। তাই তোমাদের মধ্যে যে এ উজ্জ্বলতা বাড়িয়ে নিতে পারে, সে যেন তা করে।’ (মুসলিম ২/১২, হাঃ ২৪৬, আহমাদ ৯২০৬) (আধুনিক প্রকাশনীঃ ১৩৩, ইসলামী ফাউন্ডেশনঃ ১৩৮)"
  ];
  List<String> dataListsub = [
    "পরিচ্ছেদঃ ৬১/২৫. ইসলামে নুবুওয়াতের নিদর্শনাবলী।",
    "পরিচ্ছেদঃ ৬৪/৩০. খন্দকের যুদ্ধ। এ যুদ্ধকে আহযাবের যুদ্ধও বলা হয়।"
  ];
  List<String> content = [
    """
  
  আরবী الزكاة ‘যাকাত’ শব্দে র আভি ধানি ক অর্থ বৃদ্ধি ও উন্নতি ।
যাকাত শব্দের আভিধানিক আরেকটি অর্থ হয় ,التطهير
যার বাংলা অনুবাদ পবিত্র করা বা পরিশুদ্ধকরণ। ইসলামী দৃষ্টি ক োণে যাকাত সম্পদ পরি শুদ্ধ করার জন্যই ব্যবহৃত হয়।
যে মন, আল-কুরআনে এসে ছে ,خُذۡ مِنۡ أَمۡوَٰلِهِمۡ صَدَقَةٗ تُطَهِّرُهُمۡ وَتُزَكِّيهِم بِهَا ١٠٣‘‘তাদে র সম্পদ থে কে সদাকা গ্রহণ করুন যা তাদে রকে পবি ত্র ও পরি শুদ্ধ করবে ।’’ 1আল্লাহ তা‘আলা আর ো বলে ছে ন:قَدۡ أَفۡلَحَ مَن زَكَّٰىهَا ٩
“নি ঃসন্দে হে সে সফল হয়ে ছে যে তাকে (নফসকে ) পরি শুদ্ধ করে ছে ”। 2 যাকাতে র কারণে সম্পদ বৃদ্ধি পায়, তাই যাকাতকে যাকাত বলা হয়। যাকাতে র কারণে নে কীও বর্ধি ত হয়। আরে কটি কারণ হল ো, যাকাত নফসকে কৃপণতার ন্যায় বদ অভ্যাস থে কে পবি ত্র করে , তাই অভি ধানে র দ্বি তীয় অর্থও শরঈ‘ যাকাতে পাওয়া যায়। অপর আয়াতে আল্লাহ ইরশাদ করছে ন,وَ أَقِمۡنَ ٱلصَّلَوٰةَ وَءَاتِينَ ٱلزَّكَوٰةَ وَ أَطِعۡنَ ٱ للَّه وََرَسُولَهُۚ إۥِٓنَّمَا يُرِيدُ ٱ للَّه لُِيُذۡهِبَ عَنكُمُ ٱلرِّجۡسَ أَهۡلَ ٱۡلبَۡيتِ وَيُطَهِّرَكُمۡ تَطۡهِيرٗا٣٣ ‘‘আর তে ামরা সালাত কায়ে ম কর, যাকাত আদায় কর এবং আল্লাহ ও তাঁর রাসূলে র আনুগত্য কর। আল্লাহ ত ো চান ত োমাদে র কলুষমুক্ত করে পবি ত্র-পরি চ্ছন্ন করতে ।’’ 3 শরী‘আতে র দৃষ্টি তে ‘যাকাত’ প্রয োজ্য হয় ধন-সম্পদে আল্লাহ কর্তৃক সুনি র্দি ষ্ট ও ফরযকৃত অংশ ব োঝান োর জন্য। ধন-সম্পদ থে কে এ নি র্দি ষ্ট অংশ বে র করাকে ‘যাকাত’ বলা হয় এ জন্য যে , যে সম্পদ থে কে তা নি র্ধারণ করা হল ো, যাকাতে র কারণে তা বৃদ্ধি প্রাপ্ত হয়। ইসলামী পরি ভাষায় ‘‘স্থানান্তরয োগ্য যে ক োন ো সম্পদ শরী‘আত কর্তৃক নি র্ধারি ত একটি নি সাব তথা নি র্দি ষ্ট পরি মাণে পৌঁছালে তার ওপর প্রদে য় সম্পদকে যাকাত বলে ।’’Reference:[১] সূরা আত-তাওবাহ, আয়াত: ১০৩ [২] সূরা আশ-শামস,

 আয়াত: ৯ [৩] সূরা আল-আহযাব, আয়াত: ৩৩ [৪] আল্লামা ইউসুফ কারাদাওয়ী, ইসলামে র যাকাত বি ধান
  
  """
  ];

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            elevation: 9,
            title: Text(
              title,
              style: TextStyle(fontSize: FontSize.app_bar),
            ),
            actions: [
              IconButton(
                onPressed: () => Navigator.push(context,
                    EnterExitRoute(exitPage: widget, enterPage: SearchPage())),
                //  Get.toNamed(MyRoutes.search_page),
                icon: Icon(Icons.search),
              ),
              DropDownWidget(),
            ],
          ),
          body: SingleChildScrollView(
            child: Card(
              shape: RoundedRectangleBorder(
                  side: BorderSide(color: ConstColors.card_border),
                  borderRadius: BorderRadius.circular(5)),
              margin: const EdgeInsets.all(16.0),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // page content
                    Container(
                      margin: EdgeInsets.all(16),
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: dataList.map((String e) {
                          if (e[0] == 'a') {
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: Column(
                                children: [
                                  Text(
                                    e.substring(1),
                                    textAlign: TextAlign.justify,
                                    textDirection: TextDirection.rtl,
                                    style: TextStyle(
                                      color: ConstColors.card_text,
                                      fontFamily: 'me_quran',
                                      fontSize: FontSize.defaultsize,
                                    ),
                                  ),
                                  Expanded(
                                    child: ListView.builder(
                                      itemCount: 2,
                                      itemBuilder: (context, index) {
                                        return ListTile(
                                          title: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 12.0),
                                            child: Text(
                                              dataListsub[index].toString(),
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontSize: FontSize.defaultsize,
                                              ),
                                            ),
                                          ),
                                          trailing: Icon(Icons.arrow_back_ios),
                                        );
                                      },
                                    ),
                                  )
                                ],
                              ),
                            );
                          }
                          return MyText(text: e);
                        }).toList(),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16),
                      color: ConstColors.card_border,
                      height: 1,
                    ),
                    Container(
                      margin: EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "[১] সূরা আত-তাওবাহ, আয়াত: ৬০",
                            style: TextStyle(color: ConstColors.reference),
                          ),
                          Text(
                            "[2] সূরা আত-তাওবাহ, আয়াত: ৬০",
                            style: TextStyle(color: ConstColors.reference),
                          ),
                          Text(
                            "[3] সূরা আত-তাওবাহ, আয়াত: ৬০",
                            style: TextStyle(color: ConstColors.reference),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
