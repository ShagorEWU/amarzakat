import 'dart:io';

import 'package:AmarZakat/Controller/status_bar.dart';
import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/page_transition.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';

import 'package:AmarZakat/views/calculator/calculator_page.dart';
import 'package:AmarZakat/views/search_page.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'fikah_zakat_page.dart';
import 'questions_page.dart';
import 'zakat_organizations_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    StatusBarController controller = Get.put(StatusBarController());
    List<Widget> categoryWidgetList = [
      InkWell(
        onTap: () => Get.toNamed(MyRoutes.jakater_hisheb_page,
            arguments: "যাকাতের হিসাব"),
        // Get.toNamed(MyRoutes.fikah_zakat_page),
        child: Padding(
          padding: const EdgeInsets.only(left: 4.0),
          child: Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: ConstColors.homeCardBorder),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/icons/0.png",
                    height: FontSize.cart_icon_size),
                SizedBox(height: 20),
                Text(
                  "যাকাতের হিসাব",
                  style:
                      TextStyle(color: ConstColors.homeCardText, fontSize: 17),
                )
              ],
            ),
          ),
        ),
      ),
      InkWell(
        onTap: () {
          Get.toNamed(MyRoutes.calculator_page);
        },
        // Get.toNamed(MyRoutes.calculator_page),
        child: Padding(
          padding: const EdgeInsets.only(right: 4.0),
          child: Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
                side: BorderSide(color: ConstColors.homeCardBorder),
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/icons/1.png",
                    height: FontSize.cart_icon_size),
                SizedBox(height: 20),
                Text(
                  "যাকাত ক্যালকুলেটর",
                  style:
                      TextStyle(color: ConstColors.homeCardText, fontSize: 17),
                )
              ],
            ),
          ),
        ),
      ),
      InkWell(
        onTap: () => Get.toNamed(MyRoutes.questions_page),
        // Get.toNamed(MyRoutes.questions_page),
        child: Padding(
          padding: const EdgeInsets.only(left: 4.0),
          child: Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
                side: BorderSide(color: ConstColors.homeCardBorder),
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/icons/2.png",
                    height: FontSize.cart_icon_size),
                SizedBox(height: 20),
                Text(
                  "প্রশ্নোত্তর",
                  style:
                      TextStyle(color: ConstColors.homeCardText, fontSize: 17),
                )
              ],
            ),
          ),
        ),
      ),
      InkWell(
        onTap: () => Get.toNamed(MyRoutes.zakat_organizations_page),
        // Get.toNamed(MyRoutes.zakat_organizations_page),
        child: Padding(
          padding: const EdgeInsets.only(right: 4.0),
          child: Card(
            elevation: 3,
            shape: RoundedRectangleBorder(
                side: BorderSide(color: ConstColors.card_border),
                borderRadius: BorderRadius.circular(10)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/icons/3.png",
                    height: FontSize.cart_icon_size),
                SizedBox(height: 20),
                Text(
                  "যাকাতের প্রতিষ্ঠান",
                  style:
                      TextStyle(color: ConstColors.homeCardText, fontSize: 17),
                )
              ],
            ),
          ),
        ),
      ),
    ];

    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: WillPopScope(
        onWillPop: () {
          if (Platform.isAndroid) {
            SystemNavigator.pop();
          } else if (Platform.isIOS) {
            exit(0);
          }
        },
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: ConstColors.homeAppsBar,
              elevation: 9,
              title: Text(
                "আমার যাকাত",
                style: TextStyle(fontSize: FontSize.app_bar),
              ),
              actions: [
                IconButton(
                  onPressed: () => Navigator.push(
                      context,
                      EnterExitRoute(
                          exitPage: widget, enterPage: SearchPage())),
                  //  Get.toNamed(MyRoutes.search_page),
                  icon: Icon(Icons.search),
                ),
                DropDownWidget(),
              ],
            ),
            drawer:
                // drawerEnableOpenDragGesture: true,
                Drawer(
              child: AnimatedContainer(
                duration: Duration(seconds: 10),
                curve: Curves.bounceIn,
                child: SingleChildScrollView(
                  child: Container(
                    color: Colors.lightBlue[100],
                    child: Column(
                      children: [
                        Container(
                          height: 80,
                        ),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Container(
                            color: Colors.lightBlue[100],
                            height: 80,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 100,
                                  height: 70,
                                  decoration: BoxDecoration(
                                    // shape: BoxShape.circle,
                                    // color: Colors.white,
                                    image: DecorationImage(
                                      image: AssetImage(
                                        "assets/icons/FiqhuzZakat.png",
                                      ),

                                      //fit: BoxFit.fill
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                            color: Colors.white,
                            child: Column(children: [
                              Row(
                                children: [
                                  IconButton(
                                      icon: Icon(
                                        Icons.settings,
                                        color: Colors.blue,
                                      ),
                                      onPressed: () {}),
                                  Text("  সেটিংস")
                                ],
                              ),
                              Row(
                                children: [
                                  IconButton(
                                      icon: Icon(
                                        Icons.message,
                                        color: Colors.yellowAccent,
                                      ),
                                      onPressed: () {}),
                                  Text("  রেটিং এবং  রিভিউ")
                                ],
                              ),
                              Row(
                                children: [
                                  IconButton(
                                      icon: Icon(Icons.people_alt_sharp,
                                          color: Colors.redAccent),
                                      onPressed: () {}),
                                  Text("  আমাদের সম্পর্কে ")
                                ],
                              ),
                              Row(
                                children: [
                                  IconButton(
                                      icon: Icon(
                                        Icons.share,
                                        color: Colors.blue,
                                      ),
                                      onPressed: () {}),
                                  Text("  শেয়ার করুন")
                                ],
                              ),
                              Row(
                                children: [
                                  IconButton(
                                      icon: Icon(Icons.bookmark,
                                          color: Colors.pink),
                                      onPressed: () {}),
                                  Text("  বুকমার্ক")
                                ],
                              ),
                              Row(
                                children: [
                                  IconButton(
                                      icon: Icon(
                                        Icons.phone,
                                        color: Colors.blue,
                                      ),
                                      onPressed: () {}),
                                  Text("  যোগাযোগ ")
                                ],
                              ),
                            ])),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            body: CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: Stack(
                    children: [
                      Container(height: 220),
                      Container(
                        height: 155,
                        padding: EdgeInsets.only(top: 18),
                        color: ConstColors.homeAyatSliderBackground,
                        child: CarouselSlider(
                          options: CarouselOptions(
                            height: 175,
                            autoPlay: true,
                            viewportFraction: 1,
                          ),
                          items: [1, 2, 3, 4, 5].map((i) {
                            return Container(
                              // color: ConstColors.homeAyatSliderBackground,
                              width: MediaQuery.of(context).size.width - 16,
                              child: Text(
                                'যারা গায়েবের প্রতি ইমান আনে, সলাত কায়েম করে, এবং আমি তাদেরকে যে রিযিক দিয়েছি তা থেকে ব্যয় করে।\n[আল-বারাকাহ : আয়াত ৩]',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  height: 1.2,
                                  color: Colors.black,
                                  fontSize: FontSize.ayat,
                                  // letterSpacing: .3,
                                  // wordSpacing: .1,
                                  // fontWeight: FontWeight.bold,
                                  fontStyle: FontStyle.normal,
                                  shadows: <Shadow>[
                                    Shadow(
                                      offset: Offset(.09, .09),
                                      blurRadius: .50,
                                      color: Colors.black,
                                    ),
                                    Shadow(
                                      offset: Offset(.10, .10),
                                      blurRadius: .50,
                                      color: Colors.black,
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        left: 10,
                        right: 10,
                        child: InkWell(
                          onTap: () => Get.toNamed(MyRoutes.fikah_zakat_page),
                          // Get.toNamed(MyRoutes.fikah_zakat_page),
                          child: Container(
                            height: 150,
                            child: Card(
                              elevation: 3,
                              color: ConstColors.homeCardBackGround,
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: ConstColors.card_border),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 50.0),
                                    child: Image.asset(
                                        "assets/icons/FiqhuzZakat.png",
                                        height: 56),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 40.0),
                                    child: Text(
                                      "ফিকহুয যাকাত",
                                      style: TextStyle(
                                          fontSize: FontSize.app_bar,
                                          color: ConstColors.homeCardText),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                // SliverPadding(padding: EdgeInsets.all(10)),
                SliverPadding(
                  padding: const EdgeInsets.all(5),
                  sliver: SliverGrid(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 8,
                      mainAxisSpacing: 8,
                    ),
                    delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                      return categoryWidgetList[index];
                    }, childCount: 4),
                  ),
                ),
                SliverPadding(padding: EdgeInsets.all(10))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
