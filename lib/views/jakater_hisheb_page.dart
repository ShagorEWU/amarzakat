import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/page_transition.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:AmarZakat/utils/widgets/my_text.dart';
import 'package:AmarZakat/views/search_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class JakaterHishebPage extends StatefulWidget {
  @override
  _JakaterHishebPageState createState() => _JakaterHishebPageState();
}

class _JakaterHishebPageState extends State<JakaterHishebPage> {
  final String title = Get.arguments as String;

  List<String> dataList = [
    "আল্লাহর নাম নিয়ে  قَالَ رَجُلٌ مِنْ حَضْرَمَوْتَ مَا الْحَدَثُ يَا أَبَا (আরম্ভ করছি), (যিনি) রহমান (--পরম করুণাময়, যিনি অসীম করুণা ও দয়া বশতঃ বিশ্বজগতের সমস্ত সৃষ্টির সহাবস্থানের প্রয়োজনীয় সব ব্যবস্থা অগ্রিম করে রেখেছেন), (যিনি) রহীম (--অফুরন্ত ফলদাতা, যাঁর অপার করুণা ও দয়ার ফলে প্রত্যেকের ক্ষুদ্রতম শুভ-প্রচেষ্টাও বিপুলভাবে সাফল্যমণ্ডিত ও পুরস্কৃত হয়ে থাকে) ٱلْحَمْدُ لِلَّهِ رَبِّ ٱلْعَٰلَمِينَ সমস্ত প্রশংসা আল্লাহ্‌র প্রাপ্য, সমুদয় সৃষ্ট-জগতের রব্ব।",
    "aحَدَّثَنَا إِسْحَاقُ بْنُ إِبْرَاهِيمَ الْحَنْظَلِيُّ، قَالَ أَخْبَرَنَا عَبْدُ الرَّزَّاقِ، قَالَ أَخْبَرَنَا مَعْمَرٌ، عَنْ هَمَّامِ بْنِ مُنَبِّهٍ، أَنَّهُ سَمِعَ أَبَا هُرَيْرَةَ، يَقُولُ قَالَ رَسُولُ اللَّهِ صلى الله عليه وسلم ‏  ‏ لاَ تُقْبَلُ صَلاَةُ مَنْ أَحْدَثَ حَتَّى يَتَوَضَّأَ  .‏ قَالَ رَجُلٌ مِنْ حَضْرَمَوْتَ مَا الْحَدَثُ يَا أَبَا هُرَيْرَةَ قَالَ فُسَاءٌ أَوْ ضُرَاطٌ‏",
    "তিনি বলেনঃ আল্লাহ্‌র রসূল (সাল্লাল্লাহু ‘আলাইহি ওয়া সাল্লাম) বলেছেনঃ ‘যে ব্যক্তির হাদাস হয় তার সালাত কবুল হবে না, যতক্ষণ না সে উযূ করে। হাযরা-মাওতের জনৈক ব্যক্তি বললো, ‘হে আবূ হুরাইরা! হাদাস কী? হাদাস কী?’ তিনি বললেন, ‘নিঃশব্দে বা সশব্দে বায়ু বের হওয়া।’ \n(৬৯৫৪; মুসলিম ২/২, হাঃ ২২৫, আহমাদ ৮০৮৪) (আধুনিক প্রকাশনীঃ ১৩২, ইসলামী ফাউন্ডেশনঃ ১৩৭) ",
    "aحَدَّثَنَا يَحْيَى بْنُ بُكَيْرٍ، قَالَ حَدَّثَنَا اللَّيْثُ، عَنْ خَالِدٍ، عَنْ سَعِيدِ بْنِ أَبِي هِلاَلٍ، عَنْ نُعَيْمٍ الْمُجْمِرِ، قَالَ رَقِيتُ مَعَ أَبِي هُرَيْرَةَ عَلَى ظَهْرِ الْمَسْجِدِ، فَتَوَضَّأَ فَقَالَ إِنِّي سَمِعْتُ النَّبِيَّ صلى الله عليه وسلم يَقُولُ‏ إِنَّ أُمَّتِي يُدْعَوْنَ يَوْمَ الْقِيَامَةِ غُرًّا مُحَجَّلِينَ مِنْ آثَارِ الْوُضُوءِ، فَمَنِ اسْتَطَاعَ مِنْكُمْ أَنْ يُطِيلَ غُرَّتَهُ فَلْيَفْعَلْ",
    "নু’আয়ম মুজমির (রহঃ) থেকে বর্ণিতঃ তিনি বলেন, আমি আবূ হুরায়রা (রাঃ)-এর সঙ্গে মসজিদের ছাদে উঠলাম। অতঃপর তিনি উযূ করে বললেনঃ ‘আমি আল্লাহ্‌র রাসূল (সাল্লাল্লাহু ‘আলাইহি ওয়া সাল্লাম)-কে বলতে শুনেছি, ক্বিয়ামাতের দিন আমার উম্মাতকে এমন অবস্থায় আহ্বান করা হবে যে, উযূর প্রভাবে তাদের হাত-পা ও মুখমন্ডল উজ্জ্বল থাকবে। তাই তোমাদের মধ্যে যে এ উজ্জ্বলতা বাড়িয়ে নিতে পারে, সে যেন তা করে।’ (মুসলিম ২/১২, হাঃ ২৪৬, আহমাদ ৯২০৬) (আধুনিক প্রকাশনীঃ ১৩৩, ইসলামী ফাউন্ডেশনঃ ১৩৮)"
  ];
  List<String> dataListsub = [
    "পরিচ্ছেদঃ ৬১/২৫. ইসলামে নুবুওয়াতের নিদর্শনাবলী।",
    "পরিচ্ছেদঃ ৬৪/৩০. খন্দকের যুদ্ধ। এ যুদ্ধকে আহযাবের যুদ্ধও বলা হয়।"
  ];

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: new WillPopScope(
        onWillPop: () {
          Get.back();
        },
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Get.back();
                  }),
              elevation: 9,
              title: Text(
                title,
                style: TextStyle(fontSize: FontSize.app_bar),
              ),
              actions: [
                IconButton(
                  onPressed: () => Navigator.push(
                      context,
                      EnterExitRoute(
                          exitPage: widget, enterPage: SearchPage())),
                  //  Get.toNamed(MyRoutes.search_page),
                  icon: Icon(Icons.search),
                ),
                DropDownWidget(),
              ],
            ),
            body: Padding(
              padding: const EdgeInsets.only(top: 18.0),
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: mylist.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      Column(
                        children: [
                          Container(
                              child: ListTile(
                            hoverColor: Colors.black,
                            title: Text(
                              mylist[index][0],
                              style: TextStyle(
                                  fontSize: 19,
                                  color: ConstColors.homeCardText),
                            ),
                            trailing: IconButton(
                              icon: Icon(Icons.arrow_forward_ios),
                              onPressed: () {},
                            ),
                          )),
                        ],
                      ),
                      ListView.builder(
                        shrinkWrap: true,
                        itemCount: mylist[index].length - 1,
                        itemBuilder: (context, indext) {
                          return Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 18.0),
                                child: Container(
                                    child: ListTile(
                                  hoverColor: Colors.black,
                                  title: Text(
                                    mylist[index][indext + 1],
                                    style: TextStyle(
                                        fontSize: 17,
                                        color: ConstColors.homeCardText),
                                  ),
                                  trailing: IconButton(
                                    icon: Icon(Icons.arrow_forward_ios),
                                    onPressed: () {},
                                  ),
                                )),
                              ),
                            ],
                          );
                        },
                      ),
                    ],
                  );
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}

List<List> mylist = [
  [
    "নিসাবের পরিচয়, নিসাবের পরিমাণ ও একনজরে নিসাবের হার",
  ],
  ["মহিলাদের অলংকারের যাকাত", " স্বর্ণ ও রূপার যাকাতের নিসাব"],
  ["ব্যাংক নোটের যাকাত", "ঋণের যাকাত"],
  [
    "ব্যবসায়িক পণ্যের যাকাত",
    "ব্যাবসায়িক পণ্যে যাকাত ওয়াজিব হওয়ার শর্তসমূহ",
    "কয়েকটি জরুরি বিষয়",
    "গুরুত্বপূর্ণ কয়েকটি জ্ঞাতব্য",
    "ব্যবসায়িক পণ্যে যাকাতে র পরিমাণ",
    "ব্যবসায়ী কীভাবে ব্যবসায়ি ক পণ্য থে কে যাকাত নি র্ধারণ করবে ?",
    "ব্যবসায়ীর ফরয যাকাতের সহজ সূত্র",
  ],
  ["প্রাইজবন্ডের যাকাত"],
  [
    "শেয়ারের যাকাত",
  ],
  ["হারাম মালে র যাকাত"],
  ["চতুষ্পদ প্রাণীর যাকাত", "চতুষ্পদ প্রাণীর নি সাবে র পরি মাণ ও বর্ণনা"],
  [
    "ফল ও ফসলের যাকাত",
    "ফল ও ফসলের যাকাতের নি সাব",
    "ফসলে যাকাতের পরিমাণ",
    "ফল ও ফসলে যাকাত দেওয়ার সময়",
    "ফল ও ফসল যাকাত সংক্রান্ত বিভিন্ন মাসালা"
  ],
  ["অনুমান দ্বারা খেজুর ও আঙ্গুরের নিসাব নির্ধারণ করা"]
];
