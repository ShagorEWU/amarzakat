import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/page_transition.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:AmarZakat/utils/widgets/my_text.dart';
import 'package:AmarZakat/views/search_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ContertPage2 extends StatefulWidget {
  @override
  _ContertPage2State createState() => _ContertPage2State();
}

class _ContertPage2State extends State<ContertPage2> {
  final String title = Get.arguments as String;

  List<String> dataList = [
    "আল্লাহর নাম নিয়ে  قَالَ رَجُلٌ مِنْ حَضْرَمَوْتَ مَا الْحَدَثُ يَا أَبَا (আরম্ভ করছি), (যিনি) রহমান (--পরম করুণাময়, যিনি অসীম করুণা ও দয়া বশতঃ বিশ্বজগতের সমস্ত সৃষ্টির সহাবস্থানের প্রয়োজনীয় সব ব্যবস্থা অগ্রিম করে রেখেছেন), (যিনি) রহীম (--অফুরন্ত ফলদাতা, যাঁর অপার করুণা ও দয়ার ফলে প্রত্যেকের ক্ষুদ্রতম শুভ-প্রচেষ্টাও বিপুলভাবে সাফল্যমণ্ডিত ও পুরস্কৃত হয়ে থাকে) ٱلْحَمْدُ لِلَّهِ رَبِّ ٱلْعَٰلَمِينَ সমস্ত প্রশংসা আল্লাহ্‌র প্রাপ্য, সমুদয় সৃষ্ট-জগতের রব্ব।",
    "aحَدَّثَنَا إِسْحَاقُ بْنُ إِبْرَاهِيمَ الْحَنْظَلِيُّ، قَالَ أَخْبَرَنَا عَبْدُ الرَّزَّاقِ، قَالَ أَخْبَرَنَا مَعْمَرٌ، عَنْ هَمَّامِ بْنِ مُنَبِّهٍ، أَنَّهُ سَمِعَ أَبَا هُرَيْرَةَ، يَقُولُ قَالَ رَسُولُ اللَّهِ صلى الله عليه وسلم ‏  ‏ لاَ تُقْبَلُ صَلاَةُ مَنْ أَحْدَثَ حَتَّى يَتَوَضَّأَ  .‏ قَالَ رَجُلٌ مِنْ حَضْرَمَوْتَ مَا الْحَدَثُ يَا أَبَا هُرَيْرَةَ قَالَ فُسَاءٌ أَوْ ضُرَاطٌ‏",
    "তিনি বলেনঃ আল্লাহ্‌র রসূল (সাল্লাল্লাহু ‘আলাইহি ওয়া সাল্লাম) বলেছেনঃ ‘যে ব্যক্তির হাদাস হয় তার সালাত কবুল হবে না, যতক্ষণ না সে উযূ করে। হাযরা-মাওতের জনৈক ব্যক্তি বললো, ‘হে আবূ হুরাইরা! হাদাস কী? হাদাস কী?’ তিনি বললেন, ‘নিঃশব্দে বা সশব্দে বায়ু বের হওয়া।’ \n(৬৯৫৪; মুসলিম ২/২, হাঃ ২২৫, আহমাদ ৮০৮৪) (আধুনিক প্রকাশনীঃ ১৩২, ইসলামী ফাউন্ডেশনঃ ১৩৭) ",
    "aحَدَّثَنَا يَحْيَى بْنُ بُكَيْرٍ، قَالَ حَدَّثَنَا اللَّيْثُ، عَنْ خَالِدٍ، عَنْ سَعِيدِ بْنِ أَبِي هِلاَلٍ، عَنْ نُعَيْمٍ الْمُجْمِرِ، قَالَ رَقِيتُ مَعَ أَبِي هُرَيْرَةَ عَلَى ظَهْرِ الْمَسْجِدِ، فَتَوَضَّأَ فَقَالَ إِنِّي سَمِعْتُ النَّبِيَّ صلى الله عليه وسلم يَقُولُ‏ إِنَّ أُمَّتِي يُدْعَوْنَ يَوْمَ الْقِيَامَةِ غُرًّا مُحَجَّلِينَ مِنْ آثَارِ الْوُضُوءِ، فَمَنِ اسْتَطَاعَ مِنْكُمْ أَنْ يُطِيلَ غُرَّتَهُ فَلْيَفْعَلْ",
    "নু’আয়ম মুজমির (রহঃ) থেকে বর্ণিতঃ তিনি বলেন, আমি আবূ হুরায়রা (রাঃ)-এর সঙ্গে মসজিদের ছাদে উঠলাম। অতঃপর তিনি উযূ করে বললেনঃ ‘আমি আল্লাহ্‌র রাসূল (সাল্লাল্লাহু ‘আলাইহি ওয়া সাল্লাম)-কে বলতে শুনেছি, ক্বিয়ামাতের দিন আমার উম্মাতকে এমন অবস্থায় আহ্বান করা হবে যে, উযূর প্রভাবে তাদের হাত-পা ও মুখমন্ডল উজ্জ্বল থাকবে। তাই তোমাদের মধ্যে যে এ উজ্জ্বলতা বাড়িয়ে নিতে পারে, সে যেন তা করে।’ (মুসলিম ২/১২, হাঃ ২৪৬, আহমাদ ৯২০৬) (আধুনিক প্রকাশনীঃ ১৩৩, ইসলামী ফাউন্ডেশনঃ ১৩৮)"
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 9,
          title: Text(
            title ?? "Content 2",
            style: TextStyle(fontSize: FontSize.app_bar),
          ),
          actions: [
            IconButton(
              onPressed: () => Navigator.push(context,
                  EnterExitRoute(exitPage: widget, enterPage: SearchPage())),
              // Get.toNamed(MyRoutes.search_page),
              icon: Icon(Icons.search),
            ),
            DropDownWidget(),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // page content
              Container(
                margin: EdgeInsets.all(16),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: dataList.map((String e) {
                    if (e[0] == 'a') {
                      return Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Text(
                          e.substring(1),
                          textAlign: TextAlign.justify,
                          textDirection: TextDirection.rtl,
                          style: TextStyle(
                            color: ConstColors.card_text,
                            fontFamily: 'me_quran',
                            fontSize: FontSize.defaultsize + 2,
                          ),
                        ),
                      );
                    }
                    return MyText(text: e);
                  }).toList(),
                ),
              ),
              Container(
                padding: EdgeInsets.all(16),
                color: ConstColors.card_border,
                height: 1,
              ),
              Container(
                margin: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "[১] সূরা আত-তাওবাহ, আয়াত: ৬০",
                      style: TextStyle(color: ConstColors.reference),
                    ),
                    Text(
                      "[2] সূরা আত-তাওবাহ, আয়াত: ৬০",
                      style: TextStyle(color: ConstColors.reference),
                    ),
                    Text(
                      "[3] সূরা আত-তাওবাহ, আয়াত: ৬০",
                      style: TextStyle(color: ConstColors.reference),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
