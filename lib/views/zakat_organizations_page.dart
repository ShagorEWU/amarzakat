import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/page_transition.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:AmarZakat/views/search_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/route_manager.dart';

import 'zakat_organization_details_page.dart';

class ZakatOrganizationsPage extends StatefulWidget {
  @override
  _ZakatOrganizationsPageState createState() => _ZakatOrganizationsPageState();
}

class _ZakatOrganizationsPageState extends State<ZakatOrganizationsPage> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: new WillPopScope(
        onWillPop: () {
          Get.back();
        },
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Get.back();
                  }),
              elevation: 9,
              title: Text("যাকাতের প্রতিষ্ঠান",
                  style: TextStyle(fontSize: FontSize.app_bar)),
              actions: [
                IconButton(
                  onPressed: () => Navigator.push(
                      context,
                      EnterExitRoute(
                          exitPage: widget, enterPage: SearchPage())),
                  // Get.toNamed(MyRoutes.search_page),
                  icon: Icon(Icons.search),
                ),
                DropDownWidget(),
              ],
            ),
            body: ListView.builder(
              padding: EdgeInsets.all(16),
              itemCount: 10,
              itemBuilder: (context, index) => InkWell(
                onTap: () {
                  Get.toNamed("/ZakatOrganizationDetailsPage");
                },
                // Get.toNamed(MyRoutes.zakat_organization_details_page),
                child: Card(
                  margin: EdgeInsets.only(bottom: 16),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 200,
                        child: ClipRRect(
                            borderRadius:
                                BorderRadius.vertical(top: Radius.circular(5)),
                            child: Image(
                                //height: 100,
                                fit: BoxFit.fitWidth,
                                image: AssetImage(
                                  "assets/icons/organization.jpg",
                                ))),
                      ),
                      SizedBox(height: 15),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        child: Text("The Zakat Foundation Chicago university",
                            //overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                            maxLines: 1,
                            softWrap: true,
                            overflow: TextOverflow.ellipsis),
                      ),
                      SizedBox(height: 25),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
