import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/page_transition.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:AmarZakat/views/search_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'anewer_page.dart';

class QuestionsPage extends StatefulWidget {
  @override
  _QuestionsPageState createState() => _QuestionsPageState();
}

class _QuestionsPageState extends State<QuestionsPage> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: new WillPopScope(
        onWillPop: () {
          Get.back();
        },
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Get.back();
                  }),
              elevation: 9,
              title: Text(
                "প্রশ্নোত্তর",
                style: TextStyle(fontSize: FontSize.app_bar),
              ),
              actions: [
                IconButton(
                  onPressed: () => Navigator.push(
                      context,
                      EnterExitRoute(
                          exitPage: widget, enterPage: SearchPage())),
                  // Get.toNamed(MyRoutes.search_page),
                  icon: Icon(Icons.search),
                ),
                DropDownWidget(),
              ],
            ),
            body: ListView.builder(
              padding: EdgeInsets.all(16),
              itemBuilder: (context, index) =>
                  _Question(i: index + 1, widget: widget),
              itemCount: 30,
            ),
          ),
        ),
      ),
    );
  }
}

class _Question extends StatefulWidget {
  final int i;
  final Widget widget;

  const _Question({Key key, this.i, this.widget}) : super(key: key);

  @override
  __QuestionState createState() => __QuestionState();
}

class __QuestionState extends State<_Question> {
  String convertNumber(int eng) {
    String bengali = '';
    for (int i = 0; i < eng.toString().length; i++) {
      setState(() {
        switch (eng.toString()[i]) {
          case '1':
            bengali = bengali + '১';
            break;
          case '2':
            bengali = bengali + '২';
            break;
          case '3':
            bengali = bengali + '৩';
            break;
          case '4':
            bengali = bengali + '৪';
            break;
          case '5':
            bengali = bengali + '৫';
            break;
          case '6':
            bengali = bengali + '৬';
            break;
          case '7':
            bengali = bengali + '৭';
            break;
          case '8':
            bengali = bengali + '৮';
            break;
          case '9':
            bengali = bengali + '৯';
            break;
          default:
            bengali = bengali + '0';
        }
      });
    }
    return bengali;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Get.toNamed(MyRoutes.answer_page),
      //  Get.toNamed(MyRoutes.answer_page),
      child: Card(
        margin: EdgeInsets.only(bottom: 16),
        color: ConstColors.questionBorder,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: IntrinsicHeight(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                width: 25,
                decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.horizontal(left: Radius.circular(8))),
                child: Center(
                    child: Text(
                  convertNumber(widget.i),
                  style: TextStyle(fontSize: 20),
                )),
              ),
              Flexible(
                child: Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(8),
                  margin: EdgeInsets.all(.5),
                  child: Text(
                    widget.i % 2 == 0
                        ? """আইয়াস ইবনে মুয়াবিয়া আল-মুযানী ছিলেন দ্বিতীয় শতাব্দীর হিজরিতে একজন তাবি’ই কাদি (বিচারক) যিনি বাসরায় (আধুনিক ইরাক) বাস করতেন। তিনি প্রচুর চতুরতার অধিকারী হয়ে খ্যাতি অর্জন করেছিলেন যা আরবি লোককাহিনীর প্রিয় বিষয় হয়ে ওঠে।"""
                        : """আল্লাহর প্রিয়তম রাসূলুল্লাহ সাল্লাল্লাহু আলাইহি ওয়াসাল্লাম বলেছেন যে: “একজন মুসলমান অন্য মুসলমানের কাছে আয়না।” (আবু দাউদ)""",
                    softWrap: true,
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                        fontSize: FontSize.defaultsize,
                        height: 1.3,
                        color: Colors.black),
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.horizontal(right: Radius.circular(8))),
                child: Container(
                  width: 25,
                  child: Center(
                    child: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.white,
                      size: 18,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
