import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class CalculatorAddFieldGoldPage extends StatefulWidget {
  @override
  _CalculatorAddFieldGoldPageState createState() =>
      _CalculatorAddFieldGoldPageState();
}

class _CalculatorAddFieldGoldPageState
    extends State<CalculatorAddFieldGoldPage> {
  var _formKey = new GlobalKey<FormState>();

  int index = 0;

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: new WillPopScope(
        onWillPop: () {
          Get.back();
        },
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Get.back();
                  }),
              //backgroundColor: Colors.blue,
              elevation: 9,
              title: Text(
                "Gold",
                style: TextStyle(fontSize: FontSize.app_bar),
              ),
              actions: [
                IconButton(
                  onPressed: () {
                    Get.toNamed(MyRoutes.calculator_zakat_details_page,
                        arguments: "2");
                  },
                  icon: Icon(Icons.done),
                ),
                IconButton(
                  onPressed: () {
                    showAlertDialog(context);
                  },
                  icon: Icon(Icons.info_outline),
                ),
                DropDownWidget(),
              ],
            ),
            body: Form(
              key: _formKey,
              child: ListView(
                padding: const EdgeInsets.all(16.0),
                children: [
                  TextFormField(
                    decoration: InputDecoration(labelText: '\$ Cash in hand'),
                    keyboardType: TextInputType.number,
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    decoration: InputDecoration(labelText: '\$ Cash in hand'),
                    keyboardType: TextInputType.number,
                  ),
                ],
              ),
            ),
            bottomNavigationBar: BottomNavigationBar(
              backgroundColor: Colors.blue[700],
              selectedItemColor: Colors.white,
              onTap: (int i) {
                setState(() {
                  index = i;
                });
              },
              currentIndex: index,
              items: [
                BottomNavigationBarItem(
                  backgroundColor: Colors.white,
                  label: "weight",
                  icon: Icon(Icons.line_weight),
                ),
                BottomNavigationBarItem(
                  label: "value",
                  icon: Icon(Icons.format_list_numbered),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      // titlePadding: EdgeInsets.zero,
      title: Container(
          alignment: Alignment.topRight,
          child: InkWell(onTap: () => Get.back(), child: Icon(Icons.close))),
      content: Padding(
        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
        child: Text(
          "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.",
          textAlign: TextAlign.justify,
        ),
      ),
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
