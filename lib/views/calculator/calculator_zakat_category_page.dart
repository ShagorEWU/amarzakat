import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/page_transition.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:AmarZakat/views/calculator/calculator_add_field_gold_page.dart';
import 'package:AmarZakat/views/calculator/calculator_add_field_page.dart';
import 'package:AmarZakat/views/calculator/calculator_zakat_details_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class CalculatorZakatCategoryPage extends StatefulWidget {
  @override
  _CalculatorZakatCategoryPageState createState() =>
      _CalculatorZakatCategoryPageState();
}

class _CalculatorZakatCategoryPageState
    extends State<CalculatorZakatCategoryPage> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: new WillPopScope(
        onWillPop: () {
          Get.back();
        },
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Get.back();
                  }),
              //backgroundColor: Colors.blue,
              elevation: 9,
              title: Text(
                "যাকাত ক্যালকুলেট",
                style: TextStyle(fontSize: FontSize.app_bar),
              ),
              actions: [
                IconButton(
                  onPressed: () =>
                      Get.toNamed(MyRoutes.calculator_add_field_page),
                  // Get.toNamed(MyRoutes.calculator_zakat_details_page);

                  icon: Icon(Icons.done),
                ),
                DropDownWidget(),
              ],
            ),
            body: ListView(
              padding: EdgeInsets.all(16),
              children: [
                ListTile(
                  contentPadding: EdgeInsets.zero,
                  onTap: () => Get.toNamed(MyRoutes.calculator_add_field_page),
                  // Get.toNamed(MyRoutes.calculator_add_field_page),
                  leading: Icon(
                    Icons.attach_money,
                    size: 30,
                    color: Colors.black,
                  ),
                  title: Text(
                    "Money",
                    style: TextStyle(
                        fontSize: FontSize.defaultsize,
                        fontWeight: FontWeight.bold),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Text(
                      "0.0 BDT",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.defaultsize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  trailing: Icon(Icons.arrow_forward_ios, size: 18),
                ),
                ListTile(
                  onTap: () => Navigator.push(
                      context,
                      EnterExitRoute(
                          exitPage: widget,
                          enterPage: CalculatorAddFieldGoldPage())),
                  // Get.toNamed(MyRoutes.calculator_add_field_gold_page),
                  contentPadding: EdgeInsets.zero,
                  leading: Icon(
                    Icons.attach_money,
                    size: 30,
                    color: Colors.black,
                  ),
                  title: Text(
                    "Gold",
                    style: TextStyle(
                        fontSize: FontSize.defaultsize,
                        fontWeight: FontWeight.bold),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Text(
                      "0.0 BDT",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.defaultsize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  trailing: Icon(Icons.arrow_forward_ios, size: 18),
                ),
                ListTile(
                  contentPadding: EdgeInsets.zero,
                  leading: Icon(
                    Icons.attach_money,
                    size: 30,
                    color: Colors.black,
                  ),
                  title: Text(
                    "Silver",
                    style: TextStyle(
                        fontSize: FontSize.defaultsize,
                        fontWeight: FontWeight.bold),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Text(
                      "0.0 BDT",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.defaultsize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  trailing: Icon(Icons.arrow_forward_ios, size: 18),
                ),
                ListTile(
                  contentPadding: EdgeInsets.zero,
                  leading: Icon(
                    Icons.attach_money,
                    size: 30,
                    color: Colors.black,
                  ),
                  title: Text(
                    "Investments",
                    style: TextStyle(
                        fontSize: FontSize.defaultsize,
                        fontWeight: FontWeight.bold),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Text(
                      "0.0 BDT",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.defaultsize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  trailing: Icon(Icons.arrow_forward_ios, size: 18),
                ),
                ListTile(
                  contentPadding: EdgeInsets.zero,
                  leading: Icon(
                    Icons.attach_money,
                    size: 30,
                    color: Colors.black,
                  ),
                  title: Text(
                    "Properties",
                    style: TextStyle(
                        fontSize: FontSize.defaultsize,
                        fontWeight: FontWeight.bold),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Text(
                      "0.0 BDT",
                      style: TextStyle(
                          fontSize: FontSize.defaultsize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  trailing: Icon(Icons.arrow_forward_ios, size: 18),
                ),
                ListTile(
                  contentPadding: EdgeInsets.zero,
                  leading: Icon(
                    Icons.attach_money,
                    size: 30,
                    color: Colors.black,
                  ),
                  title: Text(
                    "Business",
                    style: TextStyle(
                        fontSize: FontSize.defaultsize,
                        fontWeight: FontWeight.bold),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Text(
                      "0.0 BDT",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.defaultsize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  trailing: Icon(Icons.arrow_forward_ios, size: 18),
                ),
                ListTile(
                  contentPadding: EdgeInsets.zero,
                  leading: Icon(
                    Icons.attach_money,
                    size: 30,
                    color: Colors.black,
                  ),
                  title: Text(
                    "Agriculture",
                    style: TextStyle(
                        fontSize: FontSize.defaultsize,
                        fontWeight: FontWeight.bold),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Text(
                      "0.0 BDT",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.defaultsize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  trailing: Icon(Icons.arrow_forward_ios, size: 18),
                ),
                ListTile(
                  contentPadding: EdgeInsets.zero,
                  leading: Icon(
                    Icons.attach_money,
                    size: 30,
                    color: Colors.black,
                  ),
                  title: Text(
                    "Cattle",
                    style: TextStyle(
                        fontSize: FontSize.defaultsize,
                        fontWeight: FontWeight.bold),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Text(
                      "0.0 BDT",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: FontSize.defaultsize,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  trailing: Icon(Icons.arrow_forward_ios, size: 18),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
