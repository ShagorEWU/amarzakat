import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/page_transition.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:AmarZakat/views/calculator/calculator_zakat_category_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:get/get.dart';

class CalculatorPage extends StatefulWidget {
  @override
  _CalculatorPageState createState() => _CalculatorPageState();
}

class _CalculatorPageState extends State<CalculatorPage> {
  final bool is_any_data = true;
  @override
  void initState() {
    //StatusBar.color(Colors.red);

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // FlutterStatusbarcolor.setStatusBarColor(Colors.white);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: new WillPopScope(
        onWillPop: () {
          Get.back();
        },
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.back();
                },
              ),

              //backgroundColor: Colors.blue, // status bar color

              elevation: 9,
              title: Text(
                "যাকাত ক্যালকুলেট",
                style: TextStyle(fontSize: FontSize.app_bar),
              ),
              actions: [
                DropDownWidget(),
              ],
            ),
            body: is_any_data
                ? ListView.builder(
                    padding: EdgeInsets.all(16),
                    itemCount: 3,
                    itemBuilder: (ctx, index) {
                      return ListTile(
                        contentPadding: EdgeInsets.zero,
                        isThreeLine: true,
                        title: Text(
                          "07/07/2020",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: FontSize.defaultsize,
                          ),
                        ),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 16),
                            Text(
                              "Total Assets =  20000 BDT",
                              style: TextStyle(
                                  fontSize: FontSize.defaultsize,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 2),
                            Text(
                              "Zakat Due = 200 BDT",
                              style: TextStyle(
                                fontSize: FontSize.defaultsize - 2,
                              ),
                            ),
                            SizedBox(height: 25),
                          ],
                        ),
                        trailing: Icon(Icons.arrow_forward_ios, size: 18),
                      );
                    },
                  )
                : Center(
                    child: Text(
                      'No previus zakat history',
                      style: TextStyle(fontSize: FontSize.defaultsize),
                    ),
                  ),
            floatingActionButton: FloatingActionButton(
              backgroundColor: Colors.black,
              onPressed: () =>
                  Get.toNamed(MyRoutes.calculator_zakat_category_page),
              // Get.toNamed(MyRoutes.calculator_zakat_category_page),
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
