import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'calculator_celender.dart';

class CalculatorZakatDetailsPage extends StatefulWidget {
  @override
  _CalculatorZakatDetailsPageState createState() =>
      _CalculatorZakatDetailsPageState();
}

class _CalculatorZakatDetailsPageState
    extends State<CalculatorZakatDetailsPage> {
  Color color1 = Colors.black12, color2 = Colors.black12;
  bool isOne = true;
  int visible = 1;
  bool isTwo = true;
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: new WillPopScope(
        onWillPop: () {
          String args = Get.arguments.toString();
          if (args == "1") {
            Get.offAllNamed(MyRoutes.calculator_add_field_page);
          } else {
            Get.offAllNamed(MyRoutes.calculator_add_field_gold_page);
          }
        },
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Get.back();
                  }),
              // backgroundColor: Colors.blue,
              elevation: 9,
              title: Text(
                "Zakat Due",
                style: TextStyle(fontSize: FontSize.app_bar),
              ),
              actions: [
                IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  // Get.toNamed(MyRoutes.calculator_zakat_details_page);

                  icon: Icon(Icons.done),
                ),
                IconButton(
                    onPressed: () {
                      setState(() {
                        visible++;
                      });
                    },
                    icon: Icon(Icons.alarm_add_outlined)),
                IconButton(
                  onPressed: () {
                    Get.defaultDialog(
                      title: "",
                      //titlePadding: EdgeInsets.zero,
                      // title: Container(
                      //     alignment: Alignment.topRight,
                      //     child: InkWell(onTap: () => Get.back(), child: Icon(Icons.close))),
                      content: Text(
                        "Are you sure you want to delete?",
                        textAlign: TextAlign.justify,
                      ),
                      actions: [
                        RaisedButton(
                            color: Colors.grey[50],
                            onPressed: () {
                              Get.back();
                            },
                            child: Text(
                              "CANCEL",
                              style: TextStyle(color: Colors.black),
                            )),
                        RaisedButton(
                            color: Colors.grey[50],
                            onPressed: () => Get.back(),
                            child: Text(
                              "DISCARD",
                              style: TextStyle(color: Colors.blue[900]),
                            ))
                      ],
                    );
                  },
                  icon: Icon(Icons.delete),
                ),
                DropDownWidget(),
              ],
            ),
            body: ListView(
              padding: EdgeInsets.all(16.0),
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 32, bottom: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Mony",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: FontSize.defaultsize),
                      ),
                      Text(
                        "2000.0BTD",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: FontSize.defaultsize),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 32),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Gold",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: FontSize.defaultsize),
                      ),
                      Text(
                        "2000.0BTD",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: FontSize.defaultsize),
                      ),
                    ],
                  ),
                ),
                Visibility(
                    visible: visible % 2 == 0 ? false : true,
                    child: Divider(
                        thickness: 1,
                        color: ConstColors.card_border,
                        height: 40)),
                Visibility(
                  visible: visible % 2 == 0 ? false : true,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 32, bottom: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Total Assets",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.defaultsize),
                        ),
                        Text(
                          "2000.0BTD",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.defaultsize),
                        ),
                      ],
                    ),
                  ),
                ),
                Visibility(
                  visible: visible % 2 == 0 ? false : true,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 32),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Zakat Due",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.defaultsize),
                        ),
                        Text(
                          "2000.0BTD",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: FontSize.defaultsize),
                        ),
                      ],
                    ),
                  ),
                ),
                Divider(
                    thickness: 1, color: ConstColors.card_border, height: 40),
                Visibility(
                  visible: visible % 2 == 0 ? false : true,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            child: Icon(Icons.alarm, color: Colors.grey),
                            onTap: () {
                              showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime.now(),
                                  lastDate: DateTime(DateTime.now().year + 5));
                            },
                          ),
                          SizedBox(width: 16),
                          Center(
                            child: Text(
                              "01/09/20",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black,
                                  fontSize: FontSize.defaultsize),
                            ),
                          ),
                          Spacer(),
                          Container(
                            width: 65,
                            child: Switch(
                                // focusColor: Colors.white,
                                // activeColor: Colors.blue,
                                activeTrackColor: Colors.lightBlue[50],
                                activeColor: Colors.blue,
                                value: isOne,
                                onChanged: (bool i) {
                                  setState(() {
                                    isOne = i;
                                  });
                                }),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            child: Icon(Icons.alarm, color: Colors.grey),
                            onTap: () {
                              showDatePicker(
                                  context: context,
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime.now(),
                                  lastDate: DateTime(DateTime.now().year + 5));
                            },
                          ),
                          SizedBox(width: 16),
                          Center(
                            child: Text(
                              "01/09/20",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black,
                                  fontSize: FontSize.defaultsize),
                            ),
                          ),
                          Spacer(),
                          Container(
                            width: 65,
                            // height: 80,
                            child: Switch(
                              activeTrackColor: Colors.lightBlue[50],
                              activeColor: Colors.blue,
                              value: isTwo,
                              onChanged: (bool i) {
                                setState(() {
                                  isTwo = i;
                                });
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Visibility(
                    visible: visible % 2 == 0 ? true : false,
                    child: Column(
                      children: [
                        CalculatorCelender(),
                        Container(
                          child: Stack(
                            children: [
                              Center(
                                  child: TextButton(
                                      onPressed: () {
                                        Get.back();
                                      },
                                      child: Text(
                                        "CANCEL",
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 20),
                                      ))),
                              Positioned(
                                  right: 5,
                                  child: TextButton(
                                      onPressed: () {
                                        Get.back();
                                      },
                                      child: Text(
                                        "OK",
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 20),
                                      ))),
                            ],
                          ),
                        )
                      ],
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the AlertDialog
    AlertDialog alert = AlertDialog();

    // show the dialog
  }
}
