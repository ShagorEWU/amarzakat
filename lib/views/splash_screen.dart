import 'package:AmarZakat/utils/my_routes.dart';
import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';

// ignore: camel_case_types
class splash_screen_widget extends StatefulWidget {
  @override
  _splash_screen_widgetState createState() => _splash_screen_widgetState();
}

// ignore: camel_case_types
class _splash_screen_widgetState extends State<splash_screen_widget> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 3), () {
      Get.toNamed(MyRoutes.home_page);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: WillPopScope(
            // ignore: missing_return
            onWillPop: () {},
            child: Container(
              color: Colors.transparent,
              //  color: Color.fromRGBO(35, 35, 35, 0.7),
              child: Stack(
                // mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Center(
                    child: Container(
                        child:
                            Image(image: AssetImage("assets/icons/logo.png"))),
                  ),
                  Positioned(
                    bottom: 16.0,
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Center(
                          child: Image(
                              width: MediaQuery.of(context).size.width / 1.5,
                              image: AssetImage(
                                  "assets/icons/taibah-Logo-Final.png")),
                        )),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
