import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/page_transition.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:AmarZakat/views/search_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class FikahZakatPage extends StatefulWidget {
  @override
  _FikahZakatPageState createState() => _FikahZakatPageState();
}

class _FikahZakatPageState extends State<FikahZakatPage> {
  List<String> dataList = [
    "যাকাতের পরিচিতি",
    "যাকাতের বিধান",
    "যাকাতের গুরুত্ব ও ফযিলত",
    "যাকাতের উপকারীতা",
    "যাকাত আদায়ে উৎসাহ প্রদান ও যাকাত আদায় না করার শাস্তি",
    "যাকাত আদায়কারীর পুরস্কার",
    "যাকাত প্রত্যাখ্যানকারীর হুকুম",
    "যাকাত ফরয হওয়ার শর্তসমূহ",
    "যাকাতের খাতসমূহ",
    "যে ধরনের সম্পদে যাকাত ওয়াজিব হয়",
    "নিসাবের পরিচয়, নিসাবের পরিমাণ ও একনজরে নিসাবের হার",
    "কিছু বইয়ের সাজেশন"
  ];

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: new WillPopScope(
        onWillPop: () {
          Get.back();
        },
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              elevation: 9,
              title: Text(
                "ফিকহুয যাকাত",
                style: TextStyle(fontSize: FontSize.app_bar - 2.0),
              ),
              leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Get.back();
                  }),
              actions: [
                IconButton(
                  onPressed: () => Navigator.push(
                      context,
                      EnterExitRoute(
                          exitPage: widget, enterPage: SearchPage())),
                  //  Get.toNamed(MyRoutes.search_page),
                  icon: Icon(Icons.search),
                ),
                DropDownWidget(),
              ],
              // actions: [
              //   IconButton(
              //     // onPressed: () => Get.toNamed(MyRoutes.search_page),
              //     onPressed: () {
              //       Navigator.push(context,
              //           EnterExitRoute(exitPage: widget, enterPage: SearchPage()));
              //     },
              //     icon: Icon(Icons.search),
              //   ),
              //   PopupMenuButton(
              //     itemBuilder: (BuildContext bc) => [
              //       PopupMenuItem(
              //           child: Row(
              //             children: [
              //               Expanded(
              //                   child: Icon(Icons.share,
              //                       color: ConstColors.popUpShare)),
              //               Expanded(child: Text("শেয়ার"))
              //             ],
              //           ),
              //           value: "/newchat"),
              //       PopupMenuItem(
              //           child: Row(
              //             children: [
              //               Expanded(
              //                   child: Icon(Icons.message,
              //                       color: ConstColors.popUpShare)),
              //               Expanded(child: Text("মেসেজ"))
              //             ],
              //           ),
              //           value: "/newchat"),
              //       // PopupMenuItem(child: Text("Settings"), value: "/settings"),
              //     ],
              //     onSelected: (route) {
              //       print(route);
              //       // Note You must create respective pages for navigation
              //       Navigator.pushNamed(context, route);
              //     },
              //   ),
              //   Padding(padding: const EdgeInsets.all(10))
              // ],
            ),
            body: ListView.separated(
              separatorBuilder: (context, index) => Container(),
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              itemCount: dataList.length,
              itemBuilder: (context, int i) {
                return ListTile(
                  onTap: () {
                    print("asdfg");
                    Get.toNamed(MyRoutes.fikah_zakat_details_page, arguments: [
                      i.toString(),
                      dataList[i],
                    ]);
                    print("asdfg");
                  },
                  contentPadding: EdgeInsets.all(0),
                  title: Text(dataList[i],
                      style: TextStyle(
                          color: ConstColors.item,
                          fontSize: FontSize.defaultsize)),
                  trailing: Icon(Icons.arrow_forward_ios,
                      color: ConstColors.itemIcon, size: 24),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
