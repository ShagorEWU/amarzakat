import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/page_transition.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:AmarZakat/views/search_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class ZakatOrganizationDetailsPage extends StatefulWidget {
  @override
  _ZakatOrganizationDetailsPageState createState() =>
      _ZakatOrganizationDetailsPageState();
}

class _ZakatOrganizationDetailsPageState
    extends State<ZakatOrganizationDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: new WillPopScope(
        onWillPop: () {
          Get.back();
        },
        child: SafeArea(
          child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Get.back();
                  }),
              elevation: 9,
              title: Text(
                "যাকাতের প্রতিষ্ঠান",
                style: TextStyle(fontSize: FontSize.app_bar),
              ),
              actions: [
                IconButton(
                  onPressed: () => Navigator.push(
                      context,
                      EnterExitRoute(
                          exitPage: widget, enterPage: SearchPage())),
                  // Get.toNamed(MyRoutes.search_page),
                  icon: Icon(Icons.search),
                ),
                DropDownWidget(),
              ],
            ),
            body: SingleChildScrollView(
              child: Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                margin: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                child: Column(
                  children: [
                    Container(
                      height: 200,
                      child: ClipRRect(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(5)),
                        child: Image(
                            //height: 100,
                            fit: BoxFit.fitWidth,
                            image: AssetImage(
                              "assets/icons/organization.jpg",
                            )),
                      ),
                    ),
                    SizedBox(height: 15),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Kwait zakat house",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                          softWrap: true,
                        ),
                      ),
                    ),
                    SizedBox(height: 25),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 17),
                      child: Text(
                        "Lore ipsum Lore ipsum Lore ipsum Lore ipsum Lore ipsum Lore ipsum Lore ipsum Lore ipsum ",
                        softWrap: true,
                        textAlign: TextAlign.justify,
                        // overflow: TextOverflow.visible,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
