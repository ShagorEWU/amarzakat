import 'dart:math';

import 'package:AmarZakat/Controller/list_of_content.dart';
import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:AmarZakat/utils/font_size.dart';
import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/utils/page_transition.dart';
import 'package:AmarZakat/utils/widgets/const_widget.dart';
import 'package:AmarZakat/utils/widgets/my_text.dart';
import 'package:AmarZakat/views/search_page.dart';
import 'package:easy_rich_text/easy_rich_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:flutter/gestures.dart';

import 'fikah_zakat_page.dart';

class FikahZakatDetailsPage extends StatefulWidget {
  @override
  _FikahZakatDetailsPageState createState() => _FikahZakatDetailsPageState();
}

class _FikahZakatDetailsPageState extends State<FikahZakatDetailsPage> {
  int rt = 1;
  Future<List<String>> _fetchData() async {
    // print("ddd");
    String xxx = Get.arguments[0];
    int lastIndex = -1;
    int ss = int.parse(xxx);
    if (ss >= 0) {
      content = cntttt[ss];
    }
    // print(content);

    int indexx = 0;
    for (int i = 0; i < content.length;) {
      if (content[i] == '#') {
        i++;
        // print(i);
        contentList.add(content[i]);
        i++;
        for (int j = i; content[j] != '#'; j++) {
          contentList[contentList.length - 1] =
              contentList[contentList.length - 1] + content[j];

          i++;
          //  print(i);
        }
        i++;
        //  print(i);
        ArabicPadding[contentList.length - 1] = 1;

        // print(txt);
        //print(i);
        //print(content[i]);
      } else if (content[i] == '*') {
        i++;
        //  print(i);
        contentList.add(content[i]);
        i++;
        //  print(i);
        for (int j = i; content[j] != '*'; j++) {
          contentList[contentList.length - 1] =
              contentList[contentList.length - 1] + content[j];

          i++;
          //  print(i);
        }
        i++;
        //  print(i);
        lastIndex = contentList.length - 1;

        // print(txt);
        //print(i);
        //print(content[i]);
      } else if (content[i] == '_') {
        // print("SAGOR");
        String txt = '';
        i++;
        // print(i);
        for (int j = i; content[j] != '_'; j++) {
          txt = txt + content[j];
          i++;
          //  print(i);
        }
        i++;
        // print(i);
        superScript[lastIndex] = txt;
      } else if (content[i] == '@') {
        referenceIndex = min(referenceIndex, contentList.length);
        i++;
        //  print(i);
        contentList.add(content[i]);
        i++;
        print(i);
        for (int j = i; content[j] != '@'; j++) {
          contentList[contentList.length - 1] =
              contentList[contentList.length - 1] + content[j];

          i++;
          print(i);
        }
        i++;
        //  print(i);

        // print(txt);
        //print(i);
        //print(content[i]);
      } else {
        if (content[i] != '\n') {
          contentList.add(content[i]);
          i++;
          //  print(i);
        } else {
          break;
        }
        //print("txttt");
        String tt = '';
        for (int j = i; j < content.length; j++) {
          if (content[j] != '#' &&
              content[j] != '*' &&
              content[j] != '_' &&
              content[j] != '@') {
            contentList[contentList.length - 1] =
                contentList[contentList.length - 1] + content[j];

            i++;
          } else {
            break;
          }
        }
        // print(contentList[contentList.length - 1]);
        if (contentList[contentList.length - 1] == " " ||
            contentList[contentList.length - 1] == "  ") {
          contentList.removeLast();
        }
        if (content[i] == '\n') {
          // print(i);
          break;
        }
        if (content.length - i <= 1) {
          break;
        }
      }
    }
    //print(contentList.length + 100000);
    // for (int i = 0; i < contentList.length; i++) {
    //   print(contentList[i]);
    // }
    ++rt;
    return contentList;
  }

  Widget _bodyBuild() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
      //child: //Container(),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: contentList.length,
        itemBuilder: (context, index) {
          return ArabicPadding[index] == 1
              ? Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 20, 8),
                  child: RichText(
                    //textAlign: TextAlign.,
                    textDirection: TextDirection.rtl,
                    text: TextSpan(
                      text: contentList[index],
                      style: TextStyle(
                        color: ConstColors.homeCardText,
                        fontSize: FontSize.defaultsize,
                      ),
                    ),
                  ),
                )
              : referenceIndex == index
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(padding: const EdgeInsets.all(3)),
                        Container(
                          height: 0.5,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.black,
                        ),
                        Padding(padding: const EdgeInsets.all(3)),
                        RichText(
                          //textAlign: TextAlign.,
                          textDirection: TextDirection.ltr,
                          text: TextSpan(
                            text: contentList[index],
                            style: TextStyle(
                              color: Color.fromRGBO(33, 33, 33, 0.7),
                              fontSize: FontSize.defaultsize,
                            ),
                          ),
                        )
                      ],
                    )
                  : RichText(
                      textAlign: TextAlign.justify,
                      textDirection: TextDirection.ltr,
                      text: TextSpan(
                        style: TextStyle(
                          color: Color.fromRGBO(33, 33, 33, 0.7),
                          fontSize: FontSize.defaultsize,
                        ),
                        children: [
                          TextSpan(text: contentList[index]),
                          WidgetSpan(
                            child: Transform.translate(
                              offset: const Offset(
                                0.0,
                                -8.0,
                              ),
                              child: Text(
                                superScript[index],
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Color.fromRGBO(128, 128, 128, 0.7)),
                              ),
                            ),
                          ),
                        ],
                      ));
        },
      ),
    ); // make your ui here and use the 'data' variable
  }

  // Widget demobody() {
  //   return FutureBuilder<List<String>>(
  //       future: _fetchData(),
  //       builder: (context, snapshot) {
  //         if (snapshot.hasError) {
  //           return Container(); // widget to be shown on any error
  //         }

  //       //   return snapshot.hasData
  //       //       ? _bodyBuild(data: snapshot.data)
  //       //       : Text(
  //       //           "Loading"); // widget to be shown while data is being loaded from api call method
  //       // });
  // }

  final String title = Get.arguments[1].toString();

  List<String> contentList = new List();
  int referenceIndex;
  List<String> dataList = [
    "আল্লাহর নাম নিয়ে  قَالَ رَجُلٌ مِنْ حَضْرَمَوْتَ مَا الْحَدَثُ يَا أَبَا (আরম্ভ করছি), (যিনি) রহমান (--পরম করুণাময়, যিনি অসীম করুণা ও দয়া বশতঃ বিশ্বজগতের সমস্ত সৃষ্টির সহাবস্থানের প্রয়োজনীয় সব ব্যবস্থা অগ্রিম করে রেখেছেন), (যিনি) রহীম (--অফুরন্ত ফলদাতা, যাঁর অপার করুণা ও দয়ার ফলে প্রত্যেকের ক্ষুদ্রতম শুভ-প্রচেষ্টাও বিপুলভাবে সাফল্যমণ্ডিত ও পুরস্কৃত হয়ে থাকে) ٱلْحَمْدُ لِلَّهِ رَبِّ ٱلْعَٰلَمِينَ সমস্ত প্রশংসা আল্লাহ্‌র প্রাপ্য, সমুদয় সৃষ্ট-জগতের রব্ব।",
    "aحَدَّثَنَا إِسْحَاقُ بْنُ إِبْرَاهِيمَ الْحَنْظَلِيُّ، قَالَ أَخْبَرَنَا عَبْدُ الرَّزَّاقِ، قَالَ أَخْبَرَنَا مَعْمَرٌ، عَنْ هَمَّامِ بْنِ مُنَبِّهٍ، أَنَّهُ سَمِعَ أَبَا هُرَيْرَةَ، يَقُولُ قَالَ رَسُولُ اللَّهِ صلى الله عليه وسلم ‏  ‏ لاَ تُقْبَلُ صَلاَةُ مَنْ أَحْدَثَ حَتَّى يَتَوَضَّأَ  .‏ قَالَ رَجُلٌ مِنْ حَضْرَمَوْتَ مَا الْحَدَثُ يَا أَبَا هُرَيْرَةَ قَالَ فُسَاءٌ أَوْ ضُرَاطٌ‏",
    "তিনি বলেনঃ আল্লাহ্‌র রসূল (সাল্লাল্লাহু ‘আলাইহি ওয়া সাল্লাম) বলেছেনঃ ‘যে ব্যক্তির হাদাস হয় তার সালাত কবুল হবে না, যতক্ষণ না সে উযূ করে। হাযরা-মাওতের জনৈক ব্যক্তি বললো, ‘হে আবূ হুরাইরা! হাদাস কী? হাদাস কী?’ তিনি বললেন, ‘নিঃশব্দে বা সশব্দে বায়ু বের হওয়া।’ \n(৬৯৫৪; মুসলিম ২/২, হাঃ ২২৫, আহমাদ ৮০৮৪) (আধুনিক প্রকাশনীঃ ১৩২, ইসলামী ফাউন্ডেশনঃ ১৩৭) ",
    "aحَدَّثَنَا يَحْيَى بْنُ بُكَيْرٍ، قَالَ حَدَّثَنَا اللَّيْثُ، عَنْ خَالِدٍ، عَنْ سَعِيدِ بْنِ أَبِي هِلاَلٍ، عَنْ نُعَيْمٍ الْمُجْمِرِ، قَالَ رَقِيتُ مَعَ أَبِي هُرَيْرَةَ عَلَى ظَهْرِ الْمَسْجِدِ، فَتَوَضَّأَ فَقَالَ إِنِّي سَمِعْتُ النَّبِيَّ صلى الله عليه وسلم يَقُولُ‏ إِنَّ أُمَّتِي يُدْعَوْنَ يَوْمَ الْقِيَامَةِ غُرًّا مُحَجَّلِينَ مِنْ آثَارِ الْوُضُوءِ، فَمَنِ اسْتَطَاعَ مِنْكُمْ أَنْ يُطِيلَ غُرَّتَهُ فَلْيَفْعَلْ",
    "নু’আয়ম মুজমির (রহঃ) থেকে বর্ণিতঃ তিনি বলেন, আমি আবূ হুরায়রা (রাঃ)-এর সঙ্গে মসজিদের ছাদে উঠলাম। অতঃপর তিনি উযূ করে বললেনঃ ‘আমি আল্লাহ্‌র রাসূল (সাল্লাল্লাহু ‘আলাইহি ওয়া সাল্লাম)-কে বলতে শুনেছি, ক্বিয়ামাতের দিন আমার উম্মাতকে এমন অবস্থায় আহ্বান করা হবে যে, উযূর প্রভাবে তাদের হাত-পা ও মুখমন্ডল উজ্জ্বল থাকবে। তাই তোমাদের মধ্যে যে এ উজ্জ্বলতা বাড়িয়ে নিতে পারে, সে যেন তা করে।’ (মুসলিম ২/১২, হাঃ ২৪৬, আহমাদ ৯২০৬) (আধুনিক প্রকাশনীঃ ১৩৩, ইসলামী ফাউন্ডেশনঃ ১৩৮)"
  ];
  String content = "";

  @override
  void initState() {
    referenceIndex = 1000000;
    super.initState();
    for (int i = 0; i < ArabicPadding.length; i++) {
      ArabicPadding[i] = 0;
    }
    for (int i = 0; i < superScript.length; i++) {
      superScript[i] = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
        statusBarColor: ConstColors.homeStatusBar,
      ),
      child: new WillPopScope(
        onWillPop: () {
          Get.back();
        },
        child: SafeArea(
          child: Scaffold(
              appBar: AppBar(
                leading: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Get.back();
                    }),
                elevation: 9,
                title: Text(
                  title,
                  style: TextStyle(fontSize: FontSize.app_bar),
                ),
                actions: [
                  IconButton(
                    onPressed: () => Navigator.push(
                        context,
                        EnterExitRoute(
                            exitPage: widget, enterPage: SearchPage())),
                    //  Get.toNamed(MyRoutes.search_page),
                    icon: Icon(Icons.search),
                  ),
                  DropDownWidget(),
                ],
              ),
              body: FutureBuilder(
                future: _fetchData(),
                builder: (context, snapshot) {
                  if (snapshot.hasData == null) {
                    return Container(
                        child: Text(
                      "Loading",
                      style: TextStyle(color: Colors.black),
                    ));
                  } else {
                    return _bodyBuild();
                  }
                },
              )),
        ),
      ),
    );
  }
}

List<String> superScript = [
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  " ",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  " ",
  " ",
  " ",
  " ",
  " ",
  "",
  "",
  " ",
  " ",
  " ",
  " ",
  " ",
];
List<String> Script = [
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
];
List<int> ArabicPadding = [
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
];

List<int> SCRIPT = [
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
];
String ncontent =
    "আরবী الزكاة ‘যাকাত’ শব্দের আভি ধানিক অর্থ বৃদ্ধি ও উন্নতি । যাকাত শব্দের আভিধানিক আরেকটি অর্থ হয় ,التطهير যার বাংলা অনুবাদ পবিত্র করা বা পরিশুদ্ধকরণ। ইসলামী দৃষ্টি কোণে যাকাত সম্পদ পরিশুদ্ধ করার জন্যই ব্যবহৃত হয়। যেমন, আল-কুরআনে এসেছে , # خُذۡ مِنۡ أَمۡوَٰلِهِمۡ صَدَقَةٗ تُطَهِّرُهُمۡ وَتُزَكِّيهِم بِهَا ١٠٣ # *‘‘তাদের সম্পদ থেকে সদাকা গ্রহণ করুন যা তাদেরকে পবিত্র ও পরিশুদ্ধ করবে ।’’*_1_আল্লাহ তা‘আলা আরো বলেছে ন: # قَدۡ أَفۡلَحَ مَن زَكَّٰىهَ ٩ #*“নিঃসন্দে হে সে সফল হয়েছে যে তাকে (নফসকে ) পরিশুদ্ধ করেছে ”।*_2_যাকাতের কারণে তা বৃদ্ধি প্রাপ্ত হয়। ইসলামী পরিভাষায় ‘‘স্থানান্তরযোগ্য যে কোনো সম্পদ শরী‘আত কর্তৃক নির্ধারিত একটি নিসাব তথা নির্দিষ্ট পরিমাণে পৌঁছালে তার ওপর প্রদেয় সম্পদকে যাকাত বলে ।’’ @[১] সূরা আত-তাওবাহ, আয়াত: ১০৩@ @[২] সূরা আশ-শামস, আয়াত: ৯@";
String fcontent =
    "১. আল্লাহ তা‘আলা বলেন: # ِم ۡم ُخذ ۡن ۡ لِهِ ۡمَو ٰ ت م ُ َط َصَد ِّه ُر ُهۡم َقةٗ َأ َوت َها ُ َز ِّكيهِ ِ م ب َو َص ِّل ۡۖ َعل َّن َۡيهِ ِإ ٰوَت َك ِإ َس َك َصل ٞن َ ۗ ُهۡم َوٱ َس ِمي ٌع َعلِيٌم ١٠٣ هَّللُ لَّ # *“তাদের সম্পদ থেকে সদকা নাও, এর মাধ্যমে তাদেরকে তুমি পবিত্র ও পরিশুদ্ধ করবে এবং তাদের জন্য দো‘আ কর, নিশ্চয় তোমার দো‘আ তাদের জন্য প্রশান্তি দায়ক”।*_1_ অপর আয়াতে তিনি বলেন: # َّن ِإ ِقي َن ِإ ُمتَّ ٱل ِفي ٖت ۡ ٰ َءاَتٰى َو ُعُيو ٍن ١٥َ ءا ِخِذي َن َمٓا ُهۡم َجنَّ َرُّب ُهۡم ُهۡم ۚ نَّ ِإ لِ َك َقۡب َل َكانُواْ ٰ ا َ نوْ ٱل َما َّۡي ِل ِّمَن َقلِياٗل ذ ُمۡح ِسِني َن ١٦َ كاُ َي ١٧ۡ ه َج ُعو َن ۡس َحارِ َأۡل ٱ َوب ُهۡم ِف ُرو َن ِ َي ١٨َ وِف ٓي ۡم ۡسَتغۡ لِهِ ۡمَو ٰ ل َّسٓاِئ ِل َح ّقٞ َأ لِّ ِ َمۡح ُروم َوٱل ١٩ۡ # *“নিশ্চয় মুত্তাকিরা থাকবে জান্নাতসমূহে ও ঝর্ণাধারায়, তাদের রব তাদের যা দিবেন তা তারা খুশীতে গ্রহণকারী হবে । ইতোপূর্বে এরাই ছিল সৎকর্মশীল। রাতের সামান্য অংশই এরা ঘুমি য়ে কাটাতো। আর রাতের শেষ প্রহরে এরা ক্ষমা চাওয়ায় রত থাকত। আর তাদের সম্পদে রয়েছে প্রার্থী ও বঞ্চিতের হক”।*_2_ অপর আয়াতে তিনি বলেন: # ِذي َن ِن ُز َوٱل و َن َّ َيك َه َب ۡ ِف َّضَة ٱلذَّ ۡ ي ِل ُينِفق ِفي ُوَن َو َها اَل َوٱل ِ ا ٍب َفَب ِّش ٱ ۡر ُهم هَّلل َسب ِ َعذَ ِ ب ٖ لِيم َأ ٣٤َ أ #*“যারা সোনা ও রূপা পুঞ্জীভূত করে রাখে এবং তা আল্লাহর রাস্তায় খরচ করে না, তুমি তাদেরকে বে দনাদায়ক আযাবের সুসংবাদ দাও”।*_3_ অপর আয়াতে তিনি বলেন:# َي ِذي َن ۡح َو َسَب َّن اَل ٱل و َن َّ َي َمٓا ۡب َخلُ ِ م هَّللُ ب َءاَتٰى ُهُم ۖ ٱ ِمن َف ۡضلِ ِهۦ ُهَو َخۡيٗرا ُه ل َبۡل ُهَو َّ م ّرٞ ُه َش ۡۖ ل و َن َّ َسُي َما َطَّوقُ َب ِخل ِهۦ ُواْ ِ َيۡو َم ب ۗ َمِة ِقَي ٰ ۡ ٱل ١٨٠ #*“আল্লাহ যাদেরকে তার অনুগ্রহ থেকে যা দান করেছেন তা নিয়ে যারা কৃপণতা করে তারা যেন ধারণা না করে যে , তা তাদের জন্য কল্যাণকর, বরং তা তাদের জন্য অকল্যাণকর, যা নিয়ে তারা কৃপণতা করে ছিল, কিয়ামত দিবসে তা দিয়ে তাদের বেড়ি পরানো হবে ”।*_4_২. ইবন আব্বাস রাদি য়াল্লাহু ‘আনহু থে কে বর্ণি ত, মু‘আয ইবন জাবাল রাদি য়াল্লাহু ‘আনহুকে ইয়ামান পাঠানোর সময় রাসূলুল্লাহ সাল্লাল্লাহু আলাইহি ওয়াসাল্লাম তাকে বলেন: # »إنك تأتي قوماً من أهل الكتاب، فاْد ُع ُهم إلى شهادة أ ْن ال إله إال هللا، وأني رسول هللا، فإ ْن هم أطاعوا لذلك، فأ ْعلِ ْم ُهم أن هللا افترض عليهم خمس صلوات في كل يوم وليلة، فإ ْن هم أطاعوا لذلك، فأ ْعلِ ْم ُهم أن هللا افترض عليهم صدقة ت ِمن أغنيائهم َرّد ُؤ َخذ فت في فقرائهم، فإ ْن هم أطا ُعوا لذلك، فإَّيا َك َو َكَراِئ ُم أموالهم )يعني ال تأخذ الزكاة ِمن أفضل ُ ِق دعوة المظلوم، أموالهم وأ َحّب َها إليهم كما سيأتي(، وات فإنه ليس بينها وبين هللا ِحجاب«. #*“তুমি কি তাবিদের এক কওমের নিকট যাচ্ছ, অতএব, তাদেরকে ‘লা-ইলাহা ইল্লাল্লাহ ও আমি আল্লাহর রাসূল’ সাক্ষীর দিকে আহ্বান কর, যদি তারা এতে আনুগত্য প্রকাশ করে , তাদেরকে জানিয়ে দাও যে , প্রত্যেক দিন ও রাতে আল্লাহ তাদের ওপর পাঁচ ওয়াক্ত সালাত ফরয করেছে ন। যদি তারা এতে আনুগত্য প্রকাশ করে , তাদেরকে জানিয়ে দাও যে , আল্লাহ তাদের ওপর সদকা ফরয করেছে ন, যা তাদের ধনীদের থেকে গ্রহণ করে তাদেরই গরীবদের মাঝে বণ্টন করা হবে , যদি তারা এতে আনুগত্য প্রকাশ করে , তুমি তাদের দামী সম্পদ গ্রহণ করা পরি হার কর, (অর্থাৎ যে সম্পদ তাদের নিকট অতি উত্তম ও অধিক পছন্দনীয় যাকাত হিসেবে সেখান থেকে তুমি গ্রহণ করবে না, সামনে এ সম্পর্কে আলোচনা আসছে ), আর মজলুমের দো‘আ থেকে বাঁচ। কারণ, তার মাঝে ও আল্লাহর মাঝে কোনো পর্দা নেই”।*_5_ ৩। আল্লাহ তায়ালা বলেন, # ِذي َن ِن ُز َوٱل و َن َّ َيك َه َب ۡ ِف َّضَة ٱلذَّ ۡ ي ِل ُينِفق ِفي ُوَن َو َها اَل َوٱل ِ ا ٍب َفَب ِّش ٱ ۡر ُهم هَّلل َسب ِ َعذَ ِ ب ٖ لِيم ُيۡح ۡي َها َم ٰى َيۡو َم َأ ٣٤َ أ َعل ِفي َ َم َنارِ َج َهنَّ َو ٰى َه َفت ا ُكۡ ِ م َو ُجنُوُب ُهۡم ِج ب َبا ُه ُهۡم ُظ ُهو ُر ُهۡۖ َو ا ذَ َه َما ۡم ٰ تُ ِس َكَنز ُكۡم ۡ نفُ وقُواْ َأِل ُ َم ۡم َفذ ا ِن ُزو َن ُكنتُ َتك ٣٥ۡ # * ‘‘আর যারা সোনা রূপা (অর্থ-র্থসম্পদ) জমা করে রাখে এবং সে গুলো আল্লাহর পথে যাকাত হিসেবে ব্যয় করে না তাদেরকে যন্ত্রণাদায়ক শাস্তির সুসংবাদ দাও। কিয়ামতের দিন সে গুলো জাহান্নামের আগুনে উত্তপ্ত করা হবে এবং সে গুলো দ্বারা তাদের মুখমণ্ডল, পার্শ্বদেশ এবং পৃষ্ঠদেশে দাগ দেওয়া হবে । আর বলা হবে এগুলোই সেই সম্পদ যা তোমরা (যাকাত না দিয়ে ) পুঞ্জিভূত করে রেখেছিলে ।’’*_6_ সুতরাং, যাকাতযোগ্য সম্পদ জমিয়ে না রেখে সেটা দ্রুত দান করে দিতে হবে । ৪। যারা যাকাত আদায়ে উদাসীনতা দেখায় এবং কৃপণতা করে তাদের বিষয়ে কুরআনে করীমে কঠিন হুঁশিয়ারি দিয়ে আল্লাহ তা‘আলা বলেন, # ِذي َن ِن ُز َوٱل و َن َّ َيك َه َب ۡ ِف َّضَة ٱلذَّ ۡ ي ِل ُينِفق ِفي ُوَن َو َها اَل َوٱل ِ ا ٍب َفَب ِّش ٱ ۡر ُهم هَّلل َسب ِ َعذَ ِ ب ٖ لِيم ُيۡح ۡي َها َم ٰى َيۡو َم َأ ٣٤َ أ َعل ِفي َ َم َنارِ َج َهنَّ َو ٰى َه َفت ا ُكۡ ِ م َو ُجنُوُب ُهۡم ِج ب َبا ُه ُهۡم ُظ ُهو ُر ُهۡۖ َو ا ذَ َه َما ۡم ٰ تُ ِس َكَنز ُكۡم ۡ نفُ وقُواْ َأِل ُ َم ۡم َفذ ا ِن ُزو َن ُكنتُ َتك ٣٥ۡ # *“আর যারা সোনা ও রূপা পুঞ্জীভূত করে রাখে , আর তা আল্লাহর রাস্তায় খরচ করে না, তুমি তাদের বেদনাদায়ক আযাবের সুসংবাদ দাও। যে দিন জাহান্নামের আগুনে তা গরম করা হবে , অতঃপর তা দ্বারা তাদের কপালে , পার্শ্বে এবং পিঠে সেঁক দে ওয়া হবে । (আর বলা হবে ) ‘এটা তা-ই যা তোমরা নিজদের জন্য জমা করে রেখেছিলে । সুতরাং তোমরা যা জমা করেছিলে তার স্বাদ উপভোগ কর’’।*_7_৫। যে সম্পদের যাকাত দেওয়া হয় না, তা অবশ্যই তা গচ্ছিত মাল যদ্বারা তার মালিককে কিয়ামতের দিন শাস্তি দেওয়া হবে । যে মনটি রাসূলুল্লাহ সাল্লাল্লাহু আলাইহি ওয়াসাল্লাম থেকে বর্ণিত বিশুদ্ধ হাদীসে এসেছে । রাসূলুল্লাহ সাল্লাল্লাহু আলাইহি ওয়াসাল্লাম বলেন, # ٍر، ْح ِم َي ل ُه َصَفاِئ ُح ِم ْن َنا ف َح ْت َ لِقَيا َمِة، ُصِّ ذا َكا َن َيْو ُم اْ َ ِإاَّل ِإ ق َها، ن َها َحَّ ذ َه ٍب َواَل ِف َّض ٍة، اَل ُيَؤِّدي ِمْ » َما ِم ْن َصا ِح ِب َ ُأ َف ْي َها َعل ِفي َ ِ َم َنار ، َه َف ا ُي ْك َج َهن َوى َّ ب ُب ُه ِ َجن ُه ْ ينُ ِ َو َما َظ َو َجب ْه ُرهُ، ِعيَد ْت َب َرَد ُك ْت لَّ ُأ ُه، ُأ ل ِفي َ َدا ُرهُ َك َيْوم ا َن ٍ َف َخ ِمق ْم ِسي َن ْ لْ َأ َسَن ٍة، َأ ُه، يلَ ِ ِعَباِد، َفَي َرى َسب َضى َبْي َن الْ َحت « َّى ُيقْ ارِ لَى النَّ ِإ َّما ِة، َوِإ َجنَّ لَى الْ ِإ َّما ِإ # *“সোনা রূপার মালিক যদি এর যাকাত আদায় না করে , তবে কিয়ামতের দিন এ ধন সম্পদকে আগুনের পাত বানানো হবে এবং জাহান্নামের আগুনে তা উত্তপ্ত করা হবে । তারপর এগুলো দ্বারা তার পার্শ্ব , ললাট ও পিঠে দাগ দেওয়া হবে । যখনই ঠাণ্ডা হবে পূণরায় তা উত্তপ্ত করা হবে -এমন দিন যে দিনের পরিমাণ দুনিয়ার পঞ্চাশ হাজার বছরের সমান হবে । এভাবে বান্দার পরিণতি জান্নাত বা জাহান্নাম নির্ধারণ না হওয়া পর্যন্ত শাস্তি চলতে থাকবে ”।*_8_ ৬। রাসূলুল্লাহ সাল্লাল্লাহু আলাইহি ওয়াসাল্লাম উট, গরু ও ছাগলের মালি কদের বিষয়ে হুঁশি য়ারি আরোপ করেন যারা তাদের পশুর যাকাত আদায় করে না। তিনি তাদের জানিয়ে দেন যে , নিশ্চয় তাদেরকে কিয়ামতের দিন যাকাত না দেওয়ার কারণে শাস্তি দেওয়া হবে । রাসূলুল্লাহ সাল্লাল্লাহু আলাইহি ওয়াসাল্লাম থেকে বিশুদ্ধ সনদে বর্ণিত, তিনি বলেন, # ذ ثَّم ُخُ ُه َيْو َم الِقَيا َمِة، ُ ِن ُي َطَّوقُ بيَبَتا ل ُه َزِ ق َر َع َ َأْ ُه َيْو َم الِقَيا َمِة ُش َجا ًعا ل ُه َمالُ ث َل َ ل ْم ُيَؤِّد َز َكاَت ُه ُمِّ » َم ْن آَتاهُ هَّللاُ َمااًل، َفَ َمَت َيْأ ْي ِه ْأ لِ ْهزِ ِ ب َك َنا َمالُ َّم َيقُو ُل َأ ُ ِشْدَقْي ِه - ث ن ُز َك« ِ - َي ْعِني ب َنا َكْ َأ # *“যাকে আল্লাহ তা‘আলা সম্পদ দান করেছেন, কিন্তু সে এ যাকাত আদায় করেনি , কিয়ামতের দিন তার সম্পদকে টেকো মাথাবিশিষ্ট বিষধর সাপের আকৃতি দান করে তার গলায় মালা পরিয়ে দেওয়া হবে , সাপটি তার মুখের দুই পার্শ্ব কামড় দিয়ে বলতে থাকবে , আমি তোমার সম্পদ আমি তোমার জমাকৃত সম্পদ”।*_9_ ৭। রাসূলুল্লাহ সাল্লাল্লাহু আলাইহি ওয়াসাল্লাম আল্লাহ তা‘আলার এ বাণী তিলাওয়াত করে তাঁর উম্মতকে সাবধান করে দেন। আল্লাহ তা‘আলা বলেন, # َي ِذي َن ۡح َو َسَب َّن اَل ٱل و َن َّ َي َمٓا ۡب َخلُ ِ م هَّللُ ب َءاَتٰى ُهُم ۖ ٱ ِمن َف ۡضلِ ِهۦ ُهَو َخۡيٗرا ُه ل َبۡل ُهَو َّ م ّرٞ ُه َش ۡۖ ل و َن َّ َسُي َما َطَّوقُ َب ِخل ِهۦ ُواْ ِ َيۡو َم ب ۗ َمِة ِقَي ٰ ۡ ٱل ١٨٠ # *“আর আল্লাহ যাদেরকে তাঁর অনুগ্রহ থেকে যা দান করেছেন তা নিয়ে যারা কৃপণতা করে তারা যেন ধারণা না করে যে , তা তাদের জন্য কল্যাণকর, বরং তা তাদের জন্য অকল্যাণকর। যা নিয়ে তারা কৃপণতা করে ছিল, কিয়ামত দিবসে তা দিয়ে তাদের বেড়ি পরানো হবে । আর আসমানসমূহ ও যমীনের উত্তরাধি কার আল্লাহরই জন্য। আর তোমরা যা আমল কর সে ব্যাপারে আল্লাহ সম্যক জ্ঞাত”।*_10_ Reference: @[১] সূরা আত-তাওবাহ, আয়াত: ৩@ @[২] সূরা আয-যারি য়াত, আয়াত: ১৫-১৯@ @[৩] সূরা আত-তাওবাহ, আয়াত: ৩৪@ @[৪] সূরা আলে ইমরান, আয়াত: ১৮[৫] সহীহ বুখারী ও সহীহ মুসলি ম@ @[৬] সূরা আত-তাওবাহ, আয়াত: ৩৪-৩৫@ @[৭] সূরা আত-তাওবাহ, আয়াত: ৩৫, ৩৬@ @[৮] সহীহ মুসলিম, হাদীস নং ৯৮৭@ @[৯] সহীহ বুখারী, হাদীস নং ১৪০৩@ @[১০] সূরা আলে ইমরান, আয়াত: ১৮০@";
List<String> cntttt = [
  ncontent,
  ncontent,
  ncontent,
  ncontent,
  fcontent,
  ncontent,
  ncontent,
  ncontent,
  ncontent,
  ncontent,
  ncontent,
  ncontent,
  ncontent,
  ncontent,
];
