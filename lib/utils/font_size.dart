class FontSize {
  static const double app_bar = 22;
  static const double ayat = 15;
  static const double card = 22;
  static const double defaultsize = 17;
  static const double cart_icon_size = 48;
}
