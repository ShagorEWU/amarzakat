import 'package:AmarZakat/utils/my_routes.dart';
import 'package:AmarZakat/views/anewer_page.dart';
import 'package:AmarZakat/views/calculator/calculator_add_field_gold_page.dart';
import 'package:AmarZakat/views/calculator/calculator_add_field_page.dart';
import 'package:AmarZakat/views/calculator/calculator_page.dart';
import 'package:AmarZakat/views/calculator/calculator_zakat_category_page.dart';
import 'package:AmarZakat/views/calculator/calculator_zakat_details_page.dart';
import 'package:AmarZakat/views/fikah_zakat_details_page.dart';
import 'package:AmarZakat/views/fikah_zakat_page.dart';
import 'package:AmarZakat/views/home_page.dart';
import 'package:AmarZakat/views/jakater_hisheb_page.dart';
import 'package:AmarZakat/views/questions_page.dart';
import 'package:AmarZakat/views/search_page.dart';
import 'package:AmarZakat/views/splash_screen.dart';
import 'package:AmarZakat/views/zakat_organization_details_page.dart';
import 'package:AmarZakat/views/zakat_organizations_page.dart';
import 'package:get/get.dart';

routes() => [
      GetPage(
          name: MyRoutes.home_page,
          page: () => HomePage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.answer_page,
          page: () => AnswerPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.fikah_zakat_details_page,
          page: () => FikahZakatDetailsPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.fikah_zakat_page,
          page: () => FikahZakatPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.questions_page,
          page: () => QuestionsPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.zakat_organizations_page,
          page: () => ZakatOrganizationsPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.zakat_organization_details_page,
          page: () => ZakatOrganizationDetailsPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.calculator_page,
          page: () => CalculatorPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.calculator_zakat_category_page,
          page: () => CalculatorZakatCategoryPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.calculator_add_field_page,
          page: () => CalculatorAddFieldPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.calculator_zakat_details_page,
          page: () => CalculatorZakatDetailsPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.calculator_add_field_gold_page,
          page: () => CalculatorAddFieldGoldPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.search_page,
          page: () => SearchPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.jakater_hisheb_page,
          page: () => JakaterHishebPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.splash_screen_widget,
          page: () => splash_screen_widget(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.jakatOrganizationDetailsPage,
          page: () => ZakatOrganizationDetailsPage(),
          transitionDuration: Duration(milliseconds: 250),
          transition: Transition.zoom),
    ];
