import 'package:AmarZakat/utils/font_size.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyText extends StatelessWidget {
  final String text;

  const MyText({Key key, this.text}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: TextAlign.justify,
      textDirection: TextDirection.ltr,
      text: TextSpan(
        text: "",
        style: TextStyle(
            color: Colors.black, fontSize: FontSize.defaultsize, height: 1.3),
        children: getText(),
      ),
    );
  }

  List<InlineSpan> getText() {
    List<InlineSpan> child = text.split('').map((e) {
      if (e.codeUnitAt(0) <= 1791 && e.codeUnitAt(0) >= 1536) {
        return TextSpan(
          text: e.toString(),
          style: TextStyle(
              // fontFamily: "me_quran",
              // height: 1.7,
              // fontWeight: FontWeight.w700,
              fontSize: FontSize.defaultsize - 1),
        );
      }
      return TextSpan(
        text: e.toString(),
      );
    }).toList();
    return child;
  }
}
