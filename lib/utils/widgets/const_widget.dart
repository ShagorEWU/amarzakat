import 'package:AmarZakat/utils/consts_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Item {
  const Item(this.name, this.icon);
  final String name;
  final Icon icon;
}

List users = [
  const Item(
      'Android',
      Icon(
        Icons.share,
        color: const Color(0xFF4BB2CC),
      )),
  const Item(
      'Flutter',
      Icon(
        Icons.copy,
        color: const Color(0x6E3343A0),
      )),
];

class DropDownWidget extends StatefulWidget {
  @override
  _DropDownWidgetState createState() => _DropDownWidgetState();
}

class _DropDownWidgetState extends State<DropDownWidget> {
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      icon: Icon(Icons.more_vert),
      padding: const EdgeInsets.all(8),
      itemBuilder: (BuildContext bc) => [
        PopupMenuItem(
            child: Row(
              children: [
                Expanded(
                    child: Icon(Icons.share, color: ConstColors.popUpShare)),
                Expanded(child: Text("শেয়ার"))
              ],
            ),
            value: "/newchat"),
        PopupMenuItem(
            child: Row(
              children: [
                Expanded(
                    child: Icon(Icons.message, color: ConstColors.popUpShare)),
                Expanded(child: Text("মেসেজ"))
              ],
            ),
            value: "/newchat"),
        // PopupMenuItem(child: Text("Settings"), value: "/settings"),
      ],
      onSelected: (route) {
        print(route);
        // Note You must create respective pages for navigation
        Navigator.pushNamed(context, route);
      },
    );
  }
}
